import pwn
import os
from pwnlib import term
from ppadb.client import Client as AdbClient

# TODO: add alt + '.' for single argument history

def get_remote_file(p, filename):
    local_filename = os.path.join('/tmp', filename)
    remote_filename = os.path.join('/data/local/tmp', filename)
    
    AdbClient().devices()[0].pull(remote_filename, local_filename)

    return local_filename

def run_shrc(p, filename):
    local_filename = get_remote_file(p, filename)

    try:
        with open(local_filename, 'r') as shp:
            p.send(shp.read())
    except FileNotFoundError:
        pass

def restore_history(p, filename = '.adb_history'):
    history = []
    local_filename = get_remote_file(p, filename)
    try:
        with open(local_filename, 'r') as hfp:
            history = hfp.read().splitlines()
    except FileNotFoundError:
        pass
    return history


def save_history(p, history, filename = '.adb_history'):
    local_filename = os.path.join('/tmp', filename)
    remote_filename = os.path.join('/data/local/tmp', filename)

    history_str = '\n'.join(history)

    with open(local_filename, 'w') as hfp:
        hfp.write(history_str)

    AdbClient().devices()[0].push(local_filename, remote_filename)
    
if __name__ == '__main__': 
    p = pwn.process(['adb', 'shell'])
    run_shrc(p, '.shrc')
    term.readline.history = restore_history(p)
    p.interactive(prompt = '$ ')
    save_history(p, term.readline.history)

