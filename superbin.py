#!/usr/bin/python3

from sys import argv
import pwn
import os
from pwnlib import term
from ppadb.client import Client as AdbClient


def restore_history(p, filename, local_file = True):
    history = []
    local_filename = os.path.join('/tmp', filename)
    #remote_filename = os.path.join('/data/local/tmp', filename)
    
    if not local_file:
        AdbClient().devices()[0].pull(remote_filename, local_filename)

    try:
        with open(local_filename, 'r') as hfp:
            history = hfp.read().splitlines()
    except FileNotFoundError:
        pass
    return history


def save_history(p, history, filename, local_file = True):
    local_filename = os.path.join('/tmp', filename)
    #remote_filename = os.path.join('/data/local/tmp', filename)

    history_str = '\n'.join(history)

    with open(local_filename, 'w') as hfp:
        hfp.write(history_str)

    if not local_file:
         AdbClient().devices()[0].push(local_filename, remote_filename)
    
if __name__ == '__main__': 
    program_line = argv[1:]
    program_name = os.path.basename(argv[1])
    p = pwn.process(program_line)
    term.readline.history = restore_history(p, filename = '.{}_history'.format(program_name))
    p.interactive(prompt = '')
    save_history(p, term.readline.history, filename = '.{}_history'.format(program_name))

